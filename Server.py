import time

from flask import Flask, request, Response
import jsonpickle
import numpy as np
import cv2

from Tensorflorization import Tensorflorization
from ImageAnalyzer import ImageAnalyzer

app = Flask(__name__)

# this function recieves post request and returns a json result after analyzing the image payload
# route http posts to this method
@app.route('/api/image', methods=['POST'])
def test():
    r = request
    # convert string of image data to uint8
    nparr = np.fromstring(r.data, np.uint8)
    # decode image
    img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    img_file_path = "image.jpg"
    cv2.imwrite(img_file_path, img)
    time.sleep(2)

    jsonResult = {}
    # analyze overhanded image if it is to bright (Ueberbelichtet) or to dark (Nacht)
    # when the result is True, the image could be analyzed by TensorFlow.
    imageAnalyzable = ImageAnalyzer().analyzeImageHistogram(imagePath=img_file_path)
    if(imageAnalyzable == True):
        # image is going to be analyzed by TensorFLow with the trained images
        jsonResult = Tensorflorization().executeTensorflorization(image_name="image.jpg")
    else:
        # if the imageAnalyzable value is False, the image is to dark or to bright.
        jsonResult = {'category', 'Image to dark or to bright'}

    # the result is send back to the sender in a json object payload
    response_pickled = jsonpickle.encode(jsonResult)
    return Response(response=response_pickled, status=200, mimetype="application/json")

# start flask app
app.run(host="0.0.0.0", port=45000)