FROM python:3

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install any needed packages specified in requirements.txt
RUN pip install -r requirements.txt

ADD Server.py /
ADD Tensorflorization.py /
ADD ImageAnalyzer.py /
ADD retrained_labels.txt /
ADD retrained_graph.pb /

# Make port 45000 available to the world outside this container
EXPOSE 45000

# Run Server.py when the container launches
CMD ["python", "Server.py"]