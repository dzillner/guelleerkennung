Starten der Applikation

Um die Applikation auf einem Hostsystem starten zu können, muss die Containerplattform "Docker" auf diesem System installiert sein. 
Danach kann mit zwei Kommandos in der Konsole die Applikation gestartet werden. 
Die folgenden Befehle müssen im Projektordner des GitLab Repository ausgeführt werden.

1	Docker build -t guelleerkennung

2	Docker run -p 45000:45000 guelleerkennung


Mit Zeile 1 wird der Docker Container gebaut. Mit Zeile 2 wird der Container gestartet und der interne Port 45000 mit dem externen Port 45000 verbunden. 
Nun können Webcambilder an die Applikation gesendet werden. Die Bilder müssen über einen POST Request an die Url: „http://{DOCKER_IP}:45000/api/image“ oder „http://0.0.0.0:45000/api/image“ zur Anwendung gesendet werden. 
Die Klassifizierung des Bildes steht im Antworttext der Restanfrage. 
