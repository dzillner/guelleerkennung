import cv2
from matplotlib import pyplot as plt

class ImageAnalyzer:
    # this function creates a histogram from the overhanded image and analyzes if it is to dark/bright.
    def analyzeImageHistogram(self, imagePath):
        image = cv2.imread(imagePath)
        # filter blue, green and red parts from image.
        b, g, r = cv2.split(image)
        # create blud, green and red histogram.
        histBlue = cv2.calcHist([b], [0], None, [256], [0, 256])
        histGreen = cv2.calcHist([g], [0], None, [256], [0, 256])
        histRed = cv2.calcHist([r], [0], None, [256], [0, 256])
        # method 0 == correlation
        compareHistBlueHistGreen = cv2.compareHist(histBlue, histGreen, 0)
        compareHistBlueHistRed = cv2.compareHist(histBlue, histRed, 0)
        compareHistGreenHistRed = cv2.compareHist(histGreen, histRed, 0)
        # image is to dark or to bright.
        toBrightDarkIndicator = 0.9
        if (((compareHistBlueHistGreen + compareHistBlueHistRed + compareHistGreenHistRed) / 3) >= toBrightDarkIndicator):
            return False
        return True